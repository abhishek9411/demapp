//
//  InternetHelper.swift
//  Zevo360
//
//  Created by Abhishek Singh on 18/11/23.
//

import Network

class InternetCheck {

    static func isInternetAvailable(completionHandler: @escaping (Bool) -> Void) {
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
        let queue = DispatchQueue(label: "InternetConnectionMonitor")
        monitor.start(queue: queue)
    }
}


