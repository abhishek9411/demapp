//
//  DateAndTime.swift
//  Zevo360
//
//  Created by Abhishek Singh on 20/11/23.
//

import Foundation

class DateAndTime {
    
    static let yyyy_MM_dd_T_HH_MM_ss_z = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let yyyy_MM_dd_t_hh_mm_ss_ssssssz = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ"
    static let MMM_d_yyyy_h_mm_a = "MMM d, yyyy h:mm a"
    
    static func convertDateString(_ dateString: String, fromFormat: String, toFormat: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        if let date = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = toFormat
            let formattedDate = dateFormatter.string(from: date)
            return formattedDate
        } else {
            return nil
        }
    }
}
