//
//  Constant.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//

import Foundation
import UIKit

let kAppdelegate = UIApplication.shared.delegate as! AppDelegate


struct APIURL {
    
    static let top_headlines_API = "https://newsapi.org/v2/top-headlines?country=us&apiKey=f7735d2266ce45b899cae734372386cc"
    static let top_headlines_BBC_API = "https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=f7735d2266ce45b899cae734372386cc"
    static let top_headlines_Germany_API = "https://newsapi.org/v2/top-headlines?country=de&category=business&apiKey=f7735d2266ce45b899cae734372386cc"
    static let top_headlines_trump_API = "https://newsapi.org/v2/top-headlines?q=trump&apiKey=f7735d2266ce45b899cae734372386cc"

}

struct Constants {
   static var newsHealdings = ["Top Headlines","BBC Headlines","Germany Headlines","Trump Headlines"]
}
