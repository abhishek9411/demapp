import Foundation
import UIKit


class NetworkHelper {
    static func fetchData<T: Codable>(from urlString: String, completion: @escaping (Result<T, NetworkError>) -> Void) {
        
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidURL))
            return
        }
        
        InternetCheck.isInternetAvailable { isInternetAvailable in
            if isInternetAvailable {
                let session = URLSession.shared
                
                let task = session.dataTask(with: url) { (data, response, error) in
                    if let error = error {
                        completion(.failure(.requestFailed(error)))
                        return
                    }
                    
                    guard let data = data else {
                        completion(.failure(.invalidData))
                        return
                    }
                    
                    let decoder = JSONDecoder()
                    do {
                        let decodedData = try decoder.decode(T.self, from: data)
                        let cachedResponse = CachedURLResponse(response: response!, data: data)
                        URLCache.shared.storeCachedResponse(cachedResponse, for: URLRequest(url: url))
                        
                        completion(.success(decodedData))
                    } catch {
                        completion(.failure(.decodingFailed(error)))
                    }
                }
                
                task.resume()
            }  else {
                if let cachedResponse = URLCache.shared.cachedResponse(for: URLRequest(url: url)) {
                    let data = cachedResponse.data
                    let decoder = JSONDecoder()
                    do {
                        let decodedData = try decoder.decode(T.self, from: data)
                        completion(.success(decodedData))
                    } catch {
                        completion(.failure(.decodingFailed(error)))
                    }
                } else {
                        completion(.failure(.netWorkFailed))
                    }
            }
        }
    }
    
    static func downloadImage(from urlString: String,  completion: @escaping (UIImage) -> Void) {
        guard let url = URL(string: urlString) else {
            return
        }

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data, let image = UIImage(data: data) {
                completion(image)
            } else {
                print("Error downloading image: \(error?.localizedDescription ?? "Unknown error")")
            }
        }.resume()
    }
}

enum NetworkError: Error {
    case invalidURL
    case requestFailed(Error)
    case invalidData
    case decodingFailed(Error)
    case netWorkFailed

}

extension NetworkError: Equatable {
    static func == (lhs: NetworkError, rhs: NetworkError) -> Bool {
        switch (lhs, rhs) {
        case (.netWorkFailed, .netWorkFailed):
            return true
        case (.invalidURL, .invalidURL):
            return true
        case (.invalidData, .invalidData):
            return true
        default:
            return false
        }
    }
}
