//
//  TopHeadlinesModel.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//


import Foundation

// MARK: - TopHeadlinesModel
struct TopHeadlinesModel: Codable {
    let status: String
    let totalResults: Int
    let articles: [Article]
}

// MARK: - Article
struct Article: Codable {
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
    let source: Source?
}

// MARK: - Source
struct Source: Codable {
    let id: String?
    let name: String
}
