//
//  NewsAppHomePageViewModel.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//

import Foundation

class NewsAppHomePageViewModel {
    
    var topNewsModel : TopHeadlinesModel?
    
    func getNewsData(newsType : NewsType, completion : @escaping ((Bool) -> Void)) {
        
        NetworkHelper.fetchData(from: newsType.getNewsType()) { (result: Result<TopHeadlinesModel, NetworkError>) in
            switch result {
            case .success(let result):
                self.topNewsModel = result
                completion(true)
            case .failure(let error):
                print("Error: \(error)")
                completion(false)
            }
        }
      }

    }
