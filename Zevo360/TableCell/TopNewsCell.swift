//
//  TopNewsCell.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//

import UIKit

class TopNewsCell: UITableViewCell {
    
    @IBOutlet weak var frameView : UIView!
    @IBOutlet weak var txtLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        frameView.layer.cornerRadius = 10
        frameView.layer.borderColor = UIColor.gray.cgColor
        frameView.layer.borderWidth = 2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
