//
//  TopHeadlinesCell.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//

import UIKit

class TopHeadlinesCell: UITableViewCell {
    
    @IBOutlet weak var frameView : UIView!
    @IBOutlet weak var txtLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var imgViewheight : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.contentMode = .scaleToFill
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
