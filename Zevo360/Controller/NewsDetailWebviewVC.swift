//
//  NewsDetailWebviewVC.swift
//  Zevo360
//
//  Created by Abhishek Singh on 18/11/23.
//


import UIKit
import WebKit

class NewsDetailWebviewVC: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView!
    var urlString : String?
    var newdDetail : Article?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "News Detail"
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.frame = CGRect(x: 0, y: 74, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-44)
        view.addSubview(webView)
        
        if newdDetail?.content == nil {
                    if let url = URL(string: urlString ?? "https://www.google.com") {
                        let request = URLRequest(url: url)
                        webView.load(request)
                    }
        } else {
            let htmlString = "<html><body><h1>\(newdDetail?.title ?? "")! </h1><h2>\(newdDetail?.content ?? "")! </h2></body></html>"
            webView.loadHTMLString(htmlString, baseURL: nil)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = CGRect(x: 0, y: 74, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-44)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started loading")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("Failed to load with error: \(error.localizedDescription)")
    }
}
