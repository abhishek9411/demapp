//
//  TopHeadlinesVC.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//

import UIKit

enum NewsType : String {
    
    case TopHeadlines, BBCNews, GermanyNews, TrumpNews
    
    func getNewsType() -> String {
        switch self {
        case .TopHeadlines:
            return APIURL.top_headlines_API
        case .BBCNews:
            return APIURL.top_headlines_BBC_API
        case .GermanyNews:
            return APIURL.top_headlines_Germany_API
        case .TrumpNews:
            return APIURL.top_headlines_trump_API
        }
    }
}


class TopHeadlinesVC: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    var viewModel = NewsAppHomePageViewModel()
    var newsType = NewsType.TopHeadlines
    var textTitle : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = newsType.rawValue
        tableView.register(UINib(nibName: "TopHeadlinesCell", bundle: nil), forCellReuseIdentifier: "TopHeadlinesCell")
        
        fetchData()
    }
    
    func fetchData() {
        viewModel.getNewsData(newsType: newsType) { flag in
            DispatchQueue.main.async {
                if flag {
                    
                    self.tableView.reloadData()
                } else {
                    self.showAlert()
                }
                
            }
            
        }
    }
    
    func showAlert(){
        let alertController = UIAlertController(title: "Network Failed", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Please try again", style: .default) { (action) in
            self.fetchData()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)    }
    
}

extension TopHeadlinesVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.topNewsModel?.articles.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TopHeadlinesCell") as? TopHeadlinesCell else {
            return UITableViewCell()
        }
        cell.txtLabel.text = self.viewModel.topNewsModel?.articles[indexPath.row].title
        if let convertedDate =
            DateAndTime.convertDateString(self.viewModel.topNewsModel?.articles[indexPath.row].publishedAt ?? "", fromFormat: newsType == .BBCNews ? DateAndTime.yyyy_MM_dd_t_hh_mm_ss_ssssssz :  DateAndTime.yyyy_MM_dd_T_HH_MM_ss_z
                                          , toFormat: DateAndTime.MMM_d_yyyy_h_mm_a) {
            cell.dateLabel.text = convertedDate
        } else {
            cell.dateLabel.text = ""
        }
        cell.imgViewheight.constant = (self.viewModel.topNewsModel?.articles[indexPath.row].urlToImage?.isEmpty ?? false || self.viewModel.topNewsModel?.articles[indexPath.row].urlToImage == nil) ? 0 : 150
        cell.imgView.image = UIImage(named: "placeholder")
        DispatchQueue.global().async {
            NetworkHelper.downloadImage(from: self.viewModel.topNewsModel?.articles[indexPath.row].urlToImage ?? "") { image in
                DispatchQueue.main.async{
                    cell.imgView.image = image
                }
            }
        }
        return cell
    }
    


    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsDetailWebviewVC") as? NewsDetailWebviewVC else { return }
        controller.newdDetail = self.viewModel.topNewsModel?.articles[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
        
    }

}

