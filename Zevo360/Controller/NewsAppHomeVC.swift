//
//  NewsAppHomeVC.swift
//  Zevo360
//
//  Created by Abhishek Singh on 17/11/23.
//

import UIKit


class NewsAppHomeVC: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    var viewModel = NewsAppHomePageViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCell()
        self.navigationItem.title = "News"
    }
    
    
    func registerCell() {
        tableView.register(UINib(nibName: "TopNewsCell", bundle: nil), forCellReuseIdentifier: "TopNewsCell")

    }
    
  

}

extension NewsAppHomeVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Constants.newsHealdings.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TopNewsCell") as? TopNewsCell else {
            return UITableViewCell()
        }
        cell.txtLabel.text = Constants.newsHealdings[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TopHeadlinesVC") as? TopHeadlinesVC else { return }
        controller.newsType = indexPath.row == 0 ? .TopHeadlines :  indexPath.row == 1 ? .BBCNews :  indexPath.row == 2 ? .GermanyNews : .TrumpNews
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
