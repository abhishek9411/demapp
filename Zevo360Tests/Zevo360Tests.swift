//
//  Zevo360Tests.swift
//  Zevo360Tests
//
//  Created by Abhishek Singh on 17/11/23.
//

import XCTest
@testable import Zevo360

final class Zevo360Tests: XCTestCase {

    var viewModel: NewsAppHomePageViewModel!

    override func setUp() {
        super.setUp()
        viewModel = NewsAppHomePageViewModel()
    }

    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func testBCCNewsDataSuccess() {
        let expectation = XCTestExpectation(description: "ok")
        NetworkHelper.fetchData(from: NewsType.BBCNews.getNewsType()) { (result: Result<TopHeadlinesModel, NetworkError>) in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                expectation.fulfill()

            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }

    func testTopheadlinesApiDataSuccess() {
        let expectation = XCTestExpectation(description: "ok")
        NetworkHelper.fetchData(from: NewsType.TopHeadlines.getNewsType()) { (result: Result<TopHeadlinesModel, NetworkError>) in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                expectation.fulfill()

            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGermanyNewsApiDataSuccess() {
        let expectation = XCTestExpectation(description: "ok")
        NetworkHelper.fetchData(from: NewsType.GermanyNews.getNewsType()) { (result: Result<TopHeadlinesModel, NetworkError>) in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                expectation.fulfill()

            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testTrumpNewsApiDataSuccess() {
        let expectation = XCTestExpectation(description: "ok")
        NetworkHelper.fetchData(from: NewsType.TrumpNews.getNewsType()) { (result: Result<TopHeadlinesModel, NetworkError>) in
            switch result {
            case .success(let data):
                XCTAssertNotNil(data)
                expectation.fulfill()

            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }

}
